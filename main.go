package main

import (
	"bytes"
	"crypto/ecdh"
	"crypto/rand"
	"fmt"
)

func serverPreComputation() (*ecdh.PublicKey, *ecdh.PrivateKey) {
	curve := ecdh.P384()
	serverKey, err := curve.GenerateKey(rand.Reader)
	if err != nil {
		fmt.Println(err)
	}

	serverPubKey, err := curve.NewPublicKey(serverKey.PublicKey().Bytes())
	if err != nil {
		fmt.Println(err)
	}
	if !bytes.Equal(serverKey.PublicKey().Bytes(), serverPubKey.Bytes()) {
		fmt.Println("encoded and decoded public keys are different")
	}
	if !serverKey.PublicKey().Equal(serverPubKey) {
		fmt.Println("encoded and decoded public keys are different")
	}

	serverPrivKey, err := curve.NewPrivateKey(serverKey.Bytes())
	if err != nil {
		fmt.Println(err)
	}
	if !bytes.Equal(serverKey.Bytes(), serverPrivKey.Bytes()) {
		fmt.Println("encoded and decoded private keys are different")
	}
	if !serverKey.Equal(serverPrivKey) {
		fmt.Println("encoded and decoded private keys are different")
	}

	return serverPubKey, serverPrivKey
}

func clientComputation(serverPublicKey *ecdh.PublicKey) (*ecdh.PublicKey, []byte) {
	curve := ecdh.P384()
	clientKey, err := curve.GenerateKey(rand.Reader)
	if err != nil {
		fmt.Println(err)
	}

	clientPubKey, err := curve.NewPublicKey(clientKey.PublicKey().Bytes())
	if err != nil {
		fmt.Println(err)
	}

	clientSecret, err := clientKey.ECDH(serverPublicKey)
	if err != nil {
		fmt.Println(err)
	}

	return clientPubKey, clientSecret
}

func serverPostComputation(clientPubKey *ecdh.PublicKey, serverKey *ecdh.PrivateKey) []byte {
	serverSecret, err := serverKey.ECDH(clientPubKey)
	if err != nil {
		fmt.Println(err)
	}
	return serverSecret
}

func test(serverSecret []byte, clientSecret []byte) {

	if !bytes.Equal(clientSecret, serverSecret) {
		fmt.Println("two ECDH computations came out different")
	}
}

func main() {
	// Configuration (before processing) - precomputation
	serverPubKey, serverPrivKey := serverPreComputation()

	// Secret encryption (serverPubKey) - client side
	clientPubKey, clientSecret := clientComputation(serverPubKey) // Gerate secret

	// Secret decryption (clientPubKey, serverPrivKey) - server side
	serverSecret := serverPostComputation(clientPubKey, serverPrivKey)

	// Integrity validation (clientSecret, serverSecret) - just validate
	fmt.Println(clientPubKey.Bytes())
	test(clientSecret, serverSecret)
	fmt.Println(clientSecret)
}
