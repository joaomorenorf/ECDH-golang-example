# ECDH Golang example

I created this example restructuring the default golang test. This code use only native packages.

I divided the program into two 4 steps:
1. First one shows what must be processed before the key exchange.
2. Encrypt the secret in the client side using the server public key.
3. Decrypt the secret in the server side, it needs the client public key and the server private key.
4.  Just a test to check if the secret was exchanged correctly.

```golang
func main() {
	// Configuration (before processing) - precomputation
	serverPubKey, serverPrivKey := serverPreComputation()

	// Secret encryption (serverPubKey) - client side
	clientPubKey, clientSecret := clientComputation(serverPubKey) // Gerate secret

	// Secret decryption (clientPubKey, serverPrivKey) - server side
	serverSecret := serverPostComputation(clientPubKey, serverPrivKey)

	// Integrity validation (clientSecret, serverSecret) - just validate
	test(clientSecret, serverSecret)
}
```